import java.awt.*;

interface Actor
{

    public void paint (Graphics g);
}

class Rabbit implements Actor
{
    Cell cell = new Cell(0,0);

    public Rabbit(Cell cell)
    {
        this.cell = cell;
    }

    public void paint (Graphics g)
    {
        g.setColor(Color.WHITE);
        g.fillRect(cell.x,cell.y,cell.size,cell.size);
        g.setColor(Color.BLACK);
        g.drawRect(cell.x,cell.y,cell.size,cell.size);
    }

    public void setX (int x)
    {
        cell.x = x;
    }

    public void setY (int y)
    {
        cell.y = y;
    }
}

class Lion implements Actor
{
    Cell cell = new Cell(0,0);

    public Lion(Cell cell)
    {
        this.cell = cell;
    }

    public void paint (Graphics g)
    {
        g.setColor(Color.RED);
        g.fillRect(cell.x,cell.y,cell.size,cell.size);
        g.setColor(Color.BLACK);
        g.drawRect(cell.x,cell.y,cell.size,cell.size);
    }

    public void setX (int x)
    {
        cell.x = x;
    }

    public void setY (int y)
    {
        cell.y = y;
    }
}

class Puppy implements Actor
{
    Cell cell = new Cell(0,0);

    public Puppy(Cell cell)
    {
        this.cell = cell;
    }

    public void paint (Graphics g)
    {
        g.setColor(Color.GREEN);
        g.fillRect(cell.x,cell.y,cell.size,cell.size);
        g.setColor(Color.BLACK);
        g.drawRect(cell.x,cell.y,cell.size,cell.size);
    }

    public void setX (int x)
    {
        cell.x = x;
    }

    public void setY (int y)
    {
        cell.y = y;
    }
}
